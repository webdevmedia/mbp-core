<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin;

use Requisite\Requisite;
use Requisite\Rule\Psr4;
use Requisite\SPLAutoLoader;

class Autoloader{

	private static $autoloader;

	private static $vendorPath;

	private static $baseDir;

	/**
	 * Create the autoloader
	 *
	 * @return mixed
	 */
	public function __construct(string $pluginFile) {

		self::setBaseDir($pluginFile);
		self::setVendorPath();
		self::getRequisite();

		self::$autoloader = new SPLAutoLoader;

		self::$autoloader->addRule(
			new Psr4(
				self::getBaseDir() . 'inc',       // base directory
				__NAMESPACE__ // base namespace____
			)
		);
	}

	/**
	 * Load the Requisite library. Alternatively you can use composer's
	 */
	private static function getRequisite(){
		$declared_classes = array_flip( get_declared_classes() );

		if ( ! array_key_exists( 'Requisite\Requisite', $declared_classes ) ) {
			require_once self::getVendorPath() . 'dnaber/requisite/src/Requisite/Requisite.php';
			Requisite::init();
		}
	}

	/**
	 * @param mixed $baseDir
	 */
	public static function setBaseDir( string $pluginFile ): void {
		self::$baseDir = dirname($pluginFile) . '/';
	}

	/**
	 * @return mixed
	 */
	public static function getBaseDir(): string {
		return self::$baseDir;
	}

	private static function setVendorPath(){
		$vendor_path = (defined('VENDOR_PATH')) ? VENDOR_PATH : self::getBaseDir() . 'vendor/';

		if(is_readable($vendor_path)){
			self::$vendorPath = $vendor_path;
		}else{
			wp_die( 'You have to run "composer install" at ' . self::getBaseDir() );
		}
	}

	/**
	 * @return array
	 */
	public static function getVendorPath(): string{
		return self::$vendorPath;
	}

}
