<?php # -*- coding: utf-8 -*-
/**
 * @wordpress-plugin
 * Plugin Name:       ModuleBased Plugin
 * Plugin URI:        https://web-dev-media.de/
 * Description:       This ist the core element for Modulebasedplugins
 * Version:           1.0.0
 * Author:            web dev media UG
 * Author URI:        https://web-dev-media.de
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ModuleBasedPlugin
 * Domain Path:       /languages
 *
 * @package           StructuredContent
 */
namespace ModuleBasedPlugin;

use ModuleBasedPlugin\Handler\pluginConfig;

add_action( 'plugins_loaded', function () {

	require_once( dirname( __FILE__ ) . '/inc/autoloader.php' );

	new Autoloader( __FILE__ );

	$config = new pluginConfig();

	$config->add('plugin', [
		'baseDir' => dirname( __FILE__ ) . '/',
		'baseUrl' => plugin_dir_url( __FILE__ ),
	]);

    $baseDir = $config->getSetting( 'plugin', 'baseDir');
    $baseUrl = $config->getSetting( 'plugin', 'baseUrl');

	$config->add('plugin', [
		'assets' => $baseDir . 'assets/',
		'vendor' => $baseDir . 'vendor/',
		'name' => get_file_data(__FILE__, ['Plugin Name' => 'Plugin Name',], 'plugin')['Plugin Name'],
		'version' => get_file_data(__FILE__, ['Version' => 'Version',], 'plugin')['Version'],
		'namespace' => __NAMESPACE__
	]);

	$config->add('enqueue', [
		'styles' => [
			'pluginAdminCss' => [
				'src'       => 'assets/css/general-admin.css',
				'deps'      => NULL,
				'version'   => NULL,
				'media'     => NULL,
				'url'       => $baseUrl,
				'use@'      => 'admin',
			],
            'pluginMainCss' => [
                'src'     => 'assets/css/general.css',
                'deps'    => NULL,
                'version' => NULL,
                'media'   => NULL,
                'url'     => $baseUrl,
            ]
		],
		/*'scripts' => [
				['src' => $baseDir . 'assets/css/foo.js']
			],*/
	]);

	$pluginConfig = $config;

	Factories\Module::initModule($pluginConfig);

	$modules = Factories\Module::getModules();

	#load_plugin_textdomain( 'DellEmailCampaigns', false,       $config->getSetting('plugin', 'assets') . '/assets/languages' );
} );
